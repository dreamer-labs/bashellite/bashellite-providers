#!/usr/bin/env bash

main() {

  #local providers_tld="/opt/bashellite/providers.d";

  for dep in \
             pip3 \
             virtualenv \
             rm \
             ; do
    which ${dep} &>/dev/null \
    || {
         echo "[FAIL] Can not proceed until ${dep} is installed and accessible in path; exiting." \
         && exit 1;
       };
  done
  # If pip and virtualenv are installed, ensure pypi-downloader is installed in proper location, and functional.
  # If pypi-downloader is not installed, or broken, blow away the old one, and install a new one.
  ${providers_tld}/pypi/exec/bin/pypi-downloader --help &>/dev/null \
  || {
       echo "[WARN] pypi-downloader does NOT appear to be installed, (or it is broken); (re)installing..." \
       && rm -fr ${providers_tld}/pypi/exec/ &>/dev/null \
       && virtualenv --python=python3.6 ${providers_tld}/pypi/exec/ \
       && ${providers_tld}/pypi/exec/bin/pip3 install pypi-downloader
     };
  # Ensure pypi-downloader installed successfully
  {
    ${providers_tld}/pypi/exec/bin/pypi-downloader --help &>/dev/null \
    && echo "[INFO] pypi-downloader installed successfully...";
  } \
  || {
       echo "[FAIL] pypi-downloader was NOT installed successfully; exiting." \
       && exit 1;
     };
}

main
