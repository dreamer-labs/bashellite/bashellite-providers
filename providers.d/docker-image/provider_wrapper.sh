bashelliteProviderWrapperPodman() {

  # Set vars based on passed in vars
  config_file="${_r_metadata_tld}/repos.conf.d/${_n_repo_name}/provider.conf"
  skopeo_registry_url="${_n_repo_url#http*://}"
  mirror_tld="${_r_mirror_tld}"
  mirror_repo_name="${_n_mirror_repo_name}"
  dryrun=${_r_dryrun}

  image_name_array=()

  for line in $(cat ${config_file}); do
    utilMsg INFO "$(utilTime)" "Processing line: ${line}"
    # Separating out any username in the repo, otherwise default to a username of 'library'
    IFS=$'/\n'
    repo_line_array=( ${line%%:*} )
    unset IFS
    repo_username="library"
    orig_line=${line}
    if [ ${#repo_line_array[@]} == 2 ]; then
        repo_username=${repo_line_array[0]}
        image_name=${repo_line_array[1]}
    elif [ ${#repo_line_array[@]} == 1 ]; then
        image_name=${repo_line_array[0]}
    fi

    # Check to see if tags are listed
    tag_index=0
    tag_index=`expr index "${line}" ':'`
    tags_found=""
    tags_found="${line:${tag_index}}"

    if [[ ${tag_index} == 1 || ${tags_found} == "" ]]; then
        utilMsg WARN "$(utilTime)" "Invalid image/tag format found: ${orig_line}, skipping..."
    else
        if [[ ${tag_index} == 0 ]]; then
            # Didn't find any tags, so setting tag to 'latest'
            utilMsg INFO "$(utilTime)" "Tags not found, using 'latest' tag"
            tags_found="latest"
        else
            # Tags found
            utilMsg INFO "$(utilTime)" "Tags found"
        fi

        # Create tags array to cycle through
        IFS=$',\n'
        tags_array=( ${tags_found} )
        unset IFS

        # Here we need to make a back up of the tag array, then reset the tag array
        # and go through the copy.  If we find a regular tag, we add it to the new tag array.
        # If we find a regex, we get a list of all the tags using the dockertags helper scripts and
        # compare them against the regex.  On a match, add those tags to the new tag array.

        orig_tags_array=${tags_array[@]}
        tags_array=()

        tag_list=""

        for each_tag in ${orig_tags_array[@]}; do
            if [[ ${each_tag:0:1} != "/" ]]; then
                tags_array+=" ${each_tag}"
            else
                # Possible regex tag, check for ending delimiter
                if [[ ${each_tag: -1} != "/" ]]; then
                    utilMsg WARN "$(utilTime)" "Invalid regex tag format found: ${each_tag}, skipping..."
                else
                    # Here we have a good regex, now we download all the tags for this image if they haven't already been downloaded
                    if [[ ${tag_list} == "" ]]; then
                        # tag_list=$(${exec_dir}/dockertagsv1.sh ${repo_username}/${image_name})
                        # Here we replace the use of dockertagsv1.sh with Auth token checking and use to access the 
                        # list of tags in a v2 compliant rapid way
                        # First, do a header check to see where to get auth token
                        token=""
                        auth_info=""
                        # Here we get the auth info by making a request with an invalid token to the docker endpoint
                        while read line; do
                            if [[ ${line} =~ Www-Authenticate ]]; then
                                # We need to get auth token using info from the Www-Authenticate header...
                                auth_info=${line//*Bearer }
                                IFS=$',\n'
                                auth_info=( ${auth_info} )
                                unset IFS
                                # Here we clean up the parameters received from the previous request
                                realm=${auth_info[0]#realm=}
                                realm=${realm//\"/}
                                param1=${auth_info[1]//\"}
                                param1=${param1//:/%3A}
                                param1=${param1//\//%2F}
                                param1=${param1//$'\r'}
                                param2=${auth_info[2]//\"}
                                param2=${param2//:/%3A}
                                param2=${param2//\//%2F}
                                param2=${param2//$'\r'}
                                url="${realm}?${param1}&${param2}"
                                # Here we get a valid token that is valid for 300 sec
                                token=$(curl -s "${url}" | jq -r '.token')
                                break
                            fi
                        done < <(curl -I -s -H "Authorization: Bearer ${token}" "${_n_repo_url}/v2/${repo_username}/${image_name}/tags/list");

                        # Here we use the token to get the list of tags for the image being requested
                        tag_list=$(curl -s --request 'GET' -H "Authorization: Bearer ${token}" ${_n_repo_url}/v2/${repo_username}/${image_name}/tags/list | jq -r '.tags[]')

                    fi
                    # Next we grep the tag_list against the regex (each_tag)
                    for tag in ${tag_list}; do
                        tmpregextag=${each_tag#/}
                        regex_tag=${tmpregextag%/}
                        echo ${tag} | egrep -E "${regex_tag}" > /dev/null
                        if [[ ${?} == 0 ]]; then
                            tags_array+=" ${tag}"
                        fi
                    done
                fi
            fi
        done

        # Next we will cycle through each tag, check for an already saved file, and use skopeo to inspect the saved image and the online image and
        # compare image creation dates
        for each_tag in ${tags_array[@]}; do
            utilMsg INFO "$(utilTime)" "Processing image: ${repo_username}/${image_name}:${each_tag}..."
            save_loc="${mirror_tld}/${mirror_repo_name}"
            image_file_name="${repo_username}-${image_name}-${each_tag}.tar"
            
            # Check to see if a corresponding file exists for the image with this tag
            if [ -s ${save_loc}/${image_file_name} ]; then
              utilMsg INFO "$(utilTime)" "Previously saved file found: ${save_loc}/${image_file_name}..."
              utilMsg INFO "$(utilTime)" "Checking saved image creation timestamp..."
              local_timestamp=$(skopeo inspect docker-archive:${save_loc}/${image_file_name} | jq -r '.Created')
              utilMsg INFO "$(utilTime)" "Checking online image creation timestamp..."
              online_timestamp=$(skopeo inspect docker://${skopeo_registry_url}/${repo_username}/${image_name}:${each_tag} | jq -r '.Created')
              
              # Next with we compare the local timestamp to the online timestamp and save if online timestamp is newer
              if [[ (${local_timestamp} == ${online_timestamp}) || (${local_timestamp} > ${online_timestamp}) ]]; then
                # Newest image already saved, skipping...
                utilMsg INFO "$(utilTime)" "Saved image: ${repo_username}/${image_name}/${each_tag} is already current, skipping..."
                continue
              fi
            fi

            # Need to download and save the image
            repo_image="${skopeo_registry_url}/${repo_username}/${image_name}:${each_tag}"
            utilMsg INFO "$(utilTime)" "Saving tag: ${each_tag} for image: ${repo_username}/${image_name}"
            utilMsg INFO "$(utilTime)" "Command: skopeo copy docker://${repo_image} docker-archive:${save_loc}/${image_file_name}"
            if [[ ${dryrun} == "" ]]; then
                # Only save if not a dry run
                # # Check if file already exists, if it does, check to see if it is current
                if [ -s ${save_loc}/${image_file_name} ]; then
                    # Need to remove the old file before we can save the new one
                    rm -f ${save_loc}/${image_file_name}
                fi
                skopeo copy docker://${repo_image} docker-archive:${save_loc}/${image_file_name}
            fi
        done
    fi
  done

  # Need to clean out possible old left behind tmp files located in /var/tmp that may have been left behind by errored
  # skopeo runs
  rm -rf /var/tmp/docker-tarfile-blob*

  unset skopeo_registry_url;

}
